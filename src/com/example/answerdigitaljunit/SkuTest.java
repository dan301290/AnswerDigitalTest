package com.example.answerdigitaljunit;

import com.example.answerdigital.*;
import org.junit.*;
import static org.junit.Assert.*;
import java.math.BigDecimal;

public class SkuTest {

    private Sku a;
    private Sku b;
    private Sku c;
    private Sku d;


    @Before
    public void setUp() throws Exception {
        System.out.println("Setting it up!");
        Application.Offer tempOfferA = new HalfPriceOffer();
        Application.Offer tempOfferB = new MultipleOffer(3, new BigDecimal(1));
        Application.Offer tempOfferC = new BogofOffer(new BigDecimal(1.54).setScale(2,BigDecimal.ROUND_DOWN));
        a = new Sku("H57", "Tin o Beans", BigDecimal.valueOf(1.23), tempOfferA);
        b = new Sku("C330", "Fruity drink", BigDecimal.valueOf(0.54), tempOfferB);
        c = new Sku("BR7", "Sliced loaf", BigDecimal.valueOf(1.54), tempOfferC);
        d = new Sku("H57", "Tin o Beans", BigDecimal.valueOf(1.23), tempOfferA);
    }

    @Test
    public void testHashCode() {
        System.out.println("Running: testHashCode");
        assertTrue(a.hashCode() == d.hashCode());
        assertFalse(a.hashCode() == b.hashCode());
        d.setProductCode("YT45");
        assertFalse(a.hashCode() == d.hashCode());
    }

    @Test
    public void testEquals() {
        System.out.println("Running: testEquals");
        assertEquals(a,a);
        assertEquals(b,b);
        assertEquals(c,c);
        assertEquals(a,d);
        assertNotEquals(a,b);
        assertNotEquals(a,c);
        assertNotEquals(b,a);
        assertNotEquals(b,c);
        assertNotEquals(c,a);
        assertNotEquals(c,b);
    }

    @Test
    public void testParser() throws Exception {
        String lineA = "H57\tTin o Beans\t1.23\tHalf Price";
        String lineB = "C330\tFruity drink\t0.54\t3 for 1.00";
        String lineC = "BR7\tSliced loaf\t1.54\tBOGOF";

        Sku tempSku = new Sku();

        Sku parseTestA = tempSku.parseSku(lineA);
        Sku parseTestB = tempSku.parseSku(lineB);
        Sku parseTestC = tempSku.parseSku(lineC);

        assertEquals(a.toString(), parseTestA.toString());
        assertEquals(b.toString(), parseTestB.toString());
        assertEquals(c.toString(), parseTestC.toString());
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Running: tearDown");
        a = null;
        b = null;
        c = null;
        d = null;
        assertNull(a);
        assertNull(b);
        assertNull(c);
        assertNull(d);
    }
}