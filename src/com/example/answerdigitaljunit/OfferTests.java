package com.example.answerdigitaljunit;

import com.example.answerdigital.Application;
import com.example.answerdigital.BogofOffer;
import com.example.answerdigital.HalfPriceOffer;
import com.example.answerdigital.MultipleOffer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class OfferTests {
    private HalfPriceOffer halfPriceOfferA;
    private MultipleOffer multipleOfferA;
    private BogofOffer bogofOfferA;
    private BigDecimal result = BigDecimal.ZERO;

    @Before
    public void setUp() throws Exception {
        halfPriceOfferA = new HalfPriceOffer();
        multipleOfferA = new MultipleOffer(3, new BigDecimal(1));
        bogofOfferA = new BogofOffer(new BigDecimal(2));
    }

    @Test
    public void HalfPriceOffer(){
        result = halfPriceOfferA.totalSavings(2,new BigDecimal(10));
        assertEquals(result.toString(), "10");

        result = halfPriceOfferA.totalSavings(5,new BigDecimal(10));
        assertEquals(result.toString(), "25");

        result = halfPriceOfferA.totalSavings(5,new BigDecimal(1.5));
        assertEquals(result.toString(), "3.75");

        result = halfPriceOfferA.totalSavings(4,new BigDecimal(1.75));
        assertEquals(result.toString(), "3.50");
    }

    @Test
    public void MultipleOffer(){
        result = multipleOfferA.totalSavings(3,new BigDecimal(1));
        assertEquals(result.toString(), "2");

        result = multipleOfferA.totalSavings(5,new BigDecimal(1.75));
        assertEquals(result.toString(), "4.25");

        // Should return 0 as the offer does not apply.
        result = multipleOfferA.totalSavings(2,new BigDecimal(10));
        assertEquals(result.toString(), "0");

        result = multipleOfferA.totalSavings(1,new BigDecimal(10));
        assertEquals(result, null);

    }

    @Test
    public void BogofOffer(){
        result = bogofOfferA.totalSavings(2,new BigDecimal(10));
        assertEquals(result.toString(), "10");

        result = bogofOfferA.totalSavings(3,new BigDecimal(0.5));
        assertEquals(result.toString(), "0.5");

        // Should return 0 as the offer does not apply.
        result = bogofOfferA.totalSavings(1,new BigDecimal(0.5));
        assertEquals(result, null);



    }


    @After
    public void tearDown() throws Exception {
    }
}