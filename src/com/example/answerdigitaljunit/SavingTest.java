package com.example.answerdigitaljunit;

import com.example.answerdigital.Saving;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class SavingTest {

    private Saving a;
    private Saving b;
    private Saving c;
    private Saving d;

    @Before
    public void setUp() throws Exception {
        a = new Saving("H57", "HalfPriceOffer", new BigDecimal(1));
        b = new Saving("C330", "MultipleOffer", new BigDecimal(2));
        c = new Saving("X546", "BogofOffer", new BigDecimal(4));
        d = new Saving("H57", "HalfPriceOffer", new BigDecimal(1));

    }

    @Test
    public void testHashCode() {
        System.out.println("Running: testHashCode");
        assertTrue(a.hashCode() == d.hashCode());
        assertFalse(a.hashCode() == b.hashCode());
        d.setDescription("YT45");
        assertFalse(a.hashCode() == d.hashCode());
    }

    @Test
    public void testEquals() {
        System.out.println("Running: testEquals");
        assertEquals(a,a);
        assertEquals(b,b);
        assertEquals(c,c);
        assertEquals(a,d);
        assertNotEquals(a,b);
        assertNotEquals(a,c);
        assertNotEquals(b,a);
        assertNotEquals(b,c);
        assertNotEquals(c,a);
        assertNotEquals(c,b);
    }


    @After
    public void tearDown() throws Exception {
    }
}