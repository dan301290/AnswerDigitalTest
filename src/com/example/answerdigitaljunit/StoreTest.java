package com.example.answerdigitaljunit;

import com.example.answerdigital.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class StoreTest {

    private String correctFileName;
    private String incorrectFileName;
    private Store testStore = new Store();
    private Till till = null;
    private Receipt receipt = null;

    private Sku a;
    private Sku b;
    private Sku c;
    private Sku d;


    @Before
    public void setUp() throws Exception {
        correctFileName = "C://Users/dan30/Dropbox/Apps/Heroku/AnswerDigitalTest/src/data.txt";
        incorrectFileName = "C://Users/dan30/Dropbox/Apps/Heroku/AnswerDigitalTest/src/data.txtfsdfgsdf";
        Application.Offer tempOfferA = new HalfPriceOffer();
        Application.Offer tempOfferB = new MultipleOffer(3, new BigDecimal(1));
        Application.Offer tempOfferC = new BogofOffer(new BigDecimal(1.54).setScale(2,BigDecimal.ROUND_DOWN));
        a = new Sku("H57", "Tin o Beans", BigDecimal.valueOf(1.23), tempOfferA);
        b = new Sku("C330", "Fruity drink", BigDecimal.valueOf(0.54), tempOfferB);
        c = new Sku("BR7", "Sliced loaf", BigDecimal.valueOf(1.54), tempOfferC);
        d = new Sku("H57", "Tin o Beans", BigDecimal.valueOf(1.23), tempOfferA);
    }

    // Updating stock list from file name does not throw an error, thus store is working correctly - Further Tests in "TillTest" cover testing the data contained within the store.
    @Test
    public void testUpdateStockList() throws IOException {
        boolean IOExceptionThrown = false;
        try{
            testStore.updateStockList(correctFileName);
            till = testStore.getTill();
            receipt = till.getReceipt();
        } catch (IOException e){
            IOExceptionThrown = true;
        }
        assertTrue(till instanceof Till);
        assertTrue(receipt instanceof Receipt);
        assertFalse(IOExceptionThrown);
    }

    @After
    public void tearDown() throws Exception {
    }
}