package com.example.answerdigitaljunit;

import com.example.answerdigital.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TillTest {

    private List<Sku> skuList = new ArrayList<>();
    private Store store = new Store();
    private Till till = null;
    String correctFileName = "C://Users/dan30/Dropbox/Apps/Heroku/AnswerDigitalTest/src/data.txt";
    private Sku a;
    private Sku b;
    private Sku c;

    @Before
    public void setUp() throws Exception {
        store.updateStockList(correctFileName);
        till = store.getTill();

        Application.Offer tempOfferA = new HalfPriceOffer();
        Application.Offer tempOfferB = new MultipleOffer(3, new BigDecimal(1));
        Application.Offer tempOfferC = new BogofOffer(new BigDecimal(1.54).setScale(2,BigDecimal.ROUND_DOWN));
        a = new Sku("H57", "Tin o Beans", BigDecimal.valueOf(1.23), tempOfferA);
        b = new Sku("C330", "Fruity drink", BigDecimal.valueOf(0.54), tempOfferB);
        c = new Sku("BR7", "Sliced loaf", BigDecimal.valueOf(1.54), tempOfferC);

    }

    @Test
    public void testScanAndReceipt(){

        till.scan("H57");
        till.scan("C330");
        till.scan("BR7");
        till.scan("Y65");

        Receipt receipt = till.getReceipt();
        List<Sku> basket = receipt.getBasket();

        Sku basketSkuA = basket.get(0);
        Sku basketSkuB = basket.get(1);
        Sku basketSkuC = basket.get(2);

        assertEquals(basketSkuA.getProductCode(), "H57");
        assertEquals(basketSkuB.getProductCode(), "C330");
        assertEquals(basketSkuC.getProductCode(), "BR7");
        assertNotEquals(basketSkuA.getProductCode(), "BR7");
        assertNotEquals(basketSkuB.getProductCode(), "H57");
        assertNotEquals(basketSkuC.getProductCode(), "C330");

        // "Y65" is a made up product code, therefore the scan function should not have added it to the basket.
        for (Sku sku : basket){
            assertNotEquals(sku.getProductCode(), "Y65");
        }
    }

    @After
    public void tearDown() throws Exception {
    }
}