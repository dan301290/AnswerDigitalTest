package com.example.answerdigital;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import com.example.answerdigital.Application.Offer;

public class Sku implements Serializable{

    // variables
    private String productCode;
    private String description;
    private BigDecimal price;
    private Offer offer;

    // constructor
    public Sku () {

    }

    public Sku (String productCode, String description, BigDecimal price, Offer offer) {
        this.productCode = productCode;
        this.description = description;
        this.price = price;
        this.offer = offer;
    }

    // getter/setters
    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    // Override methods
    @Override
    public int hashCode(){
        return Objects.hash(productCode, description, price);
    }


    @Override
    public boolean equals(Object x){
        // check if objects are the same
        if (x == null) return false;
        if (x.getClass() != Sku.class) return false;
        // check fields
        Sku tempSku = (Sku)x;
        return productCode == tempSku.productCode &&
                Objects.equals(description, tempSku.description) &&
                Objects.equals(price, tempSku.price);
    }

    @Override
    public String toString() {
        return "    Sku: (" +
                "productCode: '" + productCode + '\'' +
                ", description: '" + description + '\'' +
                ", price: " + price +
                ", offer: " + offer +
                ")\n";
    }

    // Methods
    public Sku parseSku(String line) throws Exception, ArrayIndexOutOfBoundsException{
        try{
            String[] lineValues = line.split("\t+");
            String tempProductCode = lineValues[0];
            String tempDescription = lineValues[1];
            BigDecimal tempPrice = BigDecimal.valueOf(Double.parseDouble(lineValues[2]));

            // parse offer
            Offer tempOffer;
            if (3 >= lineValues.length){
                tempOffer = null;
            } else {
                tempOffer = parseOffer(lineValues[3], tempPrice);
            }

            Sku a = new Sku(tempProductCode, tempDescription, tempPrice, tempOffer);
            System.out.println("    - Added Sku: " + a.toString());
            return a;
        } catch (Exception e) {
            System.out.println("File Read Error: Please check the format of the file is correct and try again");
            e.printStackTrace();
        }
        return null;
    }

    // Takes offer string and returns valid offer type.
    public Offer parseOffer(String offerCode, BigDecimal tempPrice){

        Offer tempOffer;

        switch (offerCode) {
            case "Half Price":
                tempOffer = new HalfPriceOffer();
                break;
            case "BOGOF":
                tempOffer = new BogofOffer(tempPrice);
                break;
            default:
                // Check if result contains "for" and then break into count and temp price, else set null offer
                if (offerCode.contains("for")){
                    String[] multiOfferParts = offerCode.split("\\s+");
                    int tempCount = Integer.parseInt(multiOfferParts [0]);
                    BigDecimal tempTotalPrice = new BigDecimal(Double.parseDouble(multiOfferParts[2]));
                    tempOffer = new MultipleOffer(tempCount,tempTotalPrice);
                } else {
                    tempOffer = null;
                }
        }
        return tempOffer;
    }

}
