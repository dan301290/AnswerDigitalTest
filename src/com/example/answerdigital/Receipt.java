package com.example.answerdigital;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Receipt {


    // Variables
    private List<Sku> basket = new ArrayList<>();
    private List<Saving> savingList = new ArrayList<>();
    private BigDecimal totalPrice = BigDecimal.ZERO;
    private BigDecimal totalSaving = BigDecimal.ZERO;
    private BigDecimal totalPriceAfterSavings = BigDecimal.ZERO;

    // Constructor
    public Receipt (List<Sku> basket) {
        this.basket = basket;
        createSavingList();
        calculateTotals();

        totalPrice = totalPrice.setScale(2, BigDecimal.ROUND_UP);
        totalSaving = totalSaving.setScale(2, BigDecimal.ROUND_UP);
        totalPriceAfterSavings = totalPriceAfterSavings.setScale(2, BigDecimal.ROUND_UP);
    }

    // Look through each item in the basket and calculate the savings
    public void createSavingList(){
        // Get a version of the basket, without duplicates.
        List<Sku> basketWithoutDuplicates = basket.stream().distinct().collect(Collectors.toList());

        // Loop through the basket and calculate savings
        for (Sku sku : basketWithoutDuplicates){
            if (sku.getOffer() != null) {
                int quantity = Collections.frequency(basket, sku);
                BigDecimal totalSaving = sku.getOffer().totalSavings(quantity, sku.getPrice());
                Saving tempSaving = new Saving(sku.getProductCode(), sku.getOffer().toString(), totalSaving);
                savingList.add(tempSaving);
            }
        }
    }

    public void calculateTotals(){
        for (Sku sku : basket){
            try{
                totalPrice = totalPrice.add(sku.getPrice());
            }catch(NumberFormatException nxe){
            }
        }

        for (Saving saving:savingList){
            try{
                totalSaving = totalSaving.add(saving.getTotalSaving());
            }catch(NumberFormatException nxe){
            } catch (NullPointerException e){
                totalSaving = totalSaving;
            }
        }
        totalPriceAfterSavings = totalPrice.subtract(totalSaving);
    }

    @Override
    public String toString() {
        return "Receipt: \n" +
                "basket: \n" + basket + "\n"+
                "Saving List: \n" + savingList + "\n" +
                "Total Price: £" + totalPrice + "\n" +
                "Total Saving: -£" + totalSaving +  "\n" +
                "Total Price After Savings: £" + totalPriceAfterSavings;
    }

    public List<Sku> getBasket() {
        return basket;
    }

    public void setBasket(List<Sku> basket) {
        this.basket = basket;
    }

    public List<Saving> getSavingList() {
        return savingList;
    }

    public void setSavingList(List<Saving> savingList) {
        this.savingList = savingList;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getTotalSaving() {
        return totalSaving;
    }

    public void setTotalSaving(BigDecimal totalSaving) {
        this.totalSaving = totalSaving;
    }

    public BigDecimal getTotalPriceAfterSavings() {
        return totalPriceAfterSavings;
    }

    public void setTotalPriceAfterSavings(BigDecimal totalPriceAfterSavings) {
        this.totalPriceAfterSavings = totalPriceAfterSavings;
    }
}
