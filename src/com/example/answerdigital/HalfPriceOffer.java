package com.example.answerdigital;

import java.math.BigDecimal;
import com.example.answerdigital.Application.Offer;

public class HalfPriceOffer implements Offer {

    // Constructor
    public HalfPriceOffer () {

    }

    // Take the total quantity * price and divide by 2
    public BigDecimal totalSavings(int quantity, BigDecimal itemPrice) {

        BigDecimal totalSavings = itemPrice.multiply(new BigDecimal(quantity));
        totalSavings = totalSavings.divide(new BigDecimal(2));

        return totalSavings;
    }

    @Override
    public String toString() {
        return "HalfPriceOffer{}";
    }
}
