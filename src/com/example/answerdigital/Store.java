package com.example.answerdigital;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Store {

    // C://Users/dan30/Dropbox/Apps/Heroku/AnswerDigitalTest/src/data.txt
    private List<Sku> skuList = new ArrayList<>();
    private Sku tempSku = new Sku();

    // Constructor
    public Store() {

    }

    // read data from file and add Sku's to skuList - if file cannot be found catch error.
    public void updateStockList(String fileName)throws IOException{
        Stream<String> stream = Files.lines(Paths.get(fileName));
        stream.filter(line -> !line.startsWith("Product code"))
                .forEach(item-> {
                    try {
                        skuList.add(tempSku.parseSku(item));
                    } catch (Exception e) {
                        System.out.println("File Read Error: Please check the format of the file is correct and try again");
                    }
                });
    }

    // Return an UptoDate Till Object that contains the latest Sku list.
    public Till getTill(){
        Till till = new Till(skuList);
        return till;
    }
}
