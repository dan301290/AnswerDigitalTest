package com.example.answerdigital;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Scanner;

public class Application {

    private static Scanner scanner = new Scanner(System.in);
    private static Store store = new Store();
    private static Receipt receipt = null;

    public static void main(String args[]) throws IOException {
        mainMenu();
    }

    // The default program loop, gives the user access to the main menu of the program and handles interaction.
    public static void mainMenu() throws IOException {
        System.out.println("Choose an option: ");
        System.out.println("    1. Update the Store's Sku list (inventory) via a tab delimited file");
        System.out.println("    2. Purchase items");
        System.out.println("    3. Exit");
        System.out.println("*Please Note - only the first value entered will be taken; any extra arguments will be ignored.");
        try {
            int choice = scanner.nextInt();
            switch(choice){
                case 1: updateStockList();
                    break;
                case 2: purchaseItems();
                    break;
                case 3: exit();
                    break;
                default: System.out.println("Input Incorrect - please enter a number to make your selection.");
                    mainMenu();
                    break;
            }
        } catch (Exception e){
            System.out.println("Input Incorrect  - please enter a number to make your selection.");
            scanner.nextLine();
            mainMenu();
        }
    }

    // Deals with updating the stores stock from a tab delimited file.
    public static void updateStockList() throws IOException {

        // Move scanner to next line, ignoring extra arguments.
        scanner.nextLine();

        System.out.println("Please enter a file name including its file path, i.e");
        System.out.println("     - C://Users/dan30/Dropbox/Apps/Heroku/AnswerDigitalTest/src/data.txt");
        String fileName = scanner.next();
        System.out.println("\nReading file.");

        try {
            store.updateStockList(fileName);
            System.out.println("Sku list has been updated");
            System.out.println("Returning to the Main Menu \n");
            mainMenu();
        } catch (IOException e){
            System.out.println("Error: could not find the correct file. Try again.");
            updateStockList();
        }
    }

    // Deals with scanning items and providing a receipt.
    // i.e H57 H57 H57 C330 C330 C330 C330 C330 C330 X546 X546 X546 X546 BR7 BR7 BR7 *
    public static void purchaseItems(){
        // Move scanner to next line, ignoring extra arguments.
        scanner.nextLine();
        String productCodeEntered = "";
        // Get the latest till from the store - this ensures that the till being used has the latest Sku list(inventory).
        Till till = store.getTill();

        System.out.println("Please enter a product code?");
        System.out.println("Note - Please enter '*' to stop scanning items and to receive your receipt");

        while(!"*".equals(productCodeEntered)){
            //System.out.println("Please enter a product code");
            productCodeEntered = scanner.next();
            if("*".equals(productCodeEntered)){
                break;
            }
            till.scan(productCodeEntered);
        }
        System.out.println("\nRetrieving Receipt... \n\n");
        receipt = till.getReceipt();
        System.out.println(receipt);
    }

    // Exits the program.
    public static void exit(){
        System.exit(1);
    }

    // Offer interface
    public interface Offer {
        public BigDecimal totalSavings(int quantity, BigDecimal itemPrice);
    }
}
