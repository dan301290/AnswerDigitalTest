package com.example.answerdigital;

import java.math.BigDecimal;
import com.example.answerdigital.Application.Offer;

public class MultipleOffer implements Offer {

    int count = 0;
    BigDecimal totalPrice = new BigDecimal(0);

    public MultipleOffer (int count, BigDecimal totalPrice) {
        this.count = count;
        this.totalPrice = totalPrice;
    }

    @Override
    public BigDecimal totalSavings(int quantity, BigDecimal itemPrice) {

        if (quantity >= count-1){
            // The original price of the products without the offer
            BigDecimal originalTotalPrice = itemPrice.multiply(new BigDecimal(quantity)); //2.16

            // calculate the number of times the offer applies and the remainder
            int noOfTimesOfferApplies = quantity/count;
            int remainder = quantity%count;

            BigDecimal offerTotal = totalPrice.multiply(new BigDecimal(noOfTimesOfferApplies));
            BigDecimal remainderTotal = itemPrice.multiply(new BigDecimal(remainder));

            BigDecimal savings = offerTotal.add(remainderTotal);

            BigDecimal totalSavings = originalTotalPrice.subtract(savings);
            return totalSavings;
        } else {
            BigDecimal totalSavings = null;
            return totalSavings;
        }
    }

    @Override
    public String toString() {
        return "MultipleOffer{" +
                "count=" + count +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
