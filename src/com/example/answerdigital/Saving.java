package com.example.answerdigital;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class Saving implements Serializable {

    // variables
    private String description;
    private String savingType;
    private BigDecimal totalSaving;

    // constructor
    public Saving () {

    }

    public Saving (String description, String savingType, BigDecimal totalSaving) {
        this.description = description;
        this.savingType = savingType;
        this.totalSaving = totalSaving;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSavingType() {
        return savingType;
    }

    public void setSavingType(String savingType) {
        this.savingType = savingType;
    }

    public BigDecimal getTotalSaving() {
        return totalSaving;
    }

    public void setTotalSaving(BigDecimal totalSaving) {
        this.totalSaving = totalSaving;
    }

    @Override
    public int hashCode(){
        return Objects.hash(description, savingType, totalSaving);
    }


    @Override
    public boolean equals(Object x){
        // check if objects are the same
        if (x == null) return false;
        if (x.getClass() != Saving.class) return false;
        // check fields
        Saving tempSaving = (Saving) x;
        return description == tempSaving.description &&
                Objects.equals(savingType, tempSaving.savingType) &&
                Objects.equals(totalSaving, tempSaving.totalSaving);
    }

    @Override
    public String toString() {
        return "    Savings: (" + "    " +
                "product code: '" + description + '\'' +
                "Saving Type: '" + savingType + '\'' +
                "TotalSaving: £" + totalSaving +
                ")\n";
    }
}
