package com.example.answerdigital;

import java.util.ArrayList;
import java.util.List;

public class Till {

    private List<Sku> skuList = new ArrayList<>();
    private List<Sku> basket = new ArrayList<>();

    // constructor
    public Till (List<Sku> stockList) {
        this.skuList = stockList;

        // Debug code - Used to check that list was being updated correctly.
        //for (Sku item : this.skuList){
        //    System.out.println(item.getProductCode());
        //}
    }

    // Takes a product code and adds the relevant Sku to the basket.
    public void scan(String productCode){
        System.out.println("\nProductcode Entered :" + productCode);
        boolean itemFound = false;
        for(Sku item : skuList){
            if(item.getProductCode().toString().equals(productCode.toString())){
                itemFound = true;
                basket.add(item);
                System.out.println(item.toString() + " : Added to basket");
            }
        }
        if (itemFound == false){
            System.out.println("Error - Product not found in Sku list (inventory)");
        }
    }

    // Returns a list of the Sku objects that have been scanned and calculates the correct savings.
    public Receipt getReceipt(){
        // Create Receipt object and pass the basket.
        Receipt receipt = new Receipt(basket);
        return receipt;
    }
}
