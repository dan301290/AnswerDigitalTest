package com.example.answerdigital;

import java.math.BigDecimal;
import com.example.answerdigital.Application.Offer;

public class BogofOffer extends MultipleOffer implements Offer{

    public BogofOffer (BigDecimal totalPrice) {
        super(2, totalPrice);
    }

    @Override
    public BigDecimal totalSavings(int quantity, BigDecimal itemPrice) {
        if (quantity >= count){
            // original price
            BigDecimal originalPrice = itemPrice.multiply(new BigDecimal(quantity)); // 2.16*2.16*2.16 = 6.48
            BigDecimal totalSavings = new BigDecimal(0);

            // check if the quantity of items purchased is even or odd.
            if((quantity%2) == 0){
                //even - divide the original price by 2 to get the total savings.
                totalSavings = originalPrice.divide(new BigDecimal(count));
            } else {
                //odd - divide the original price by 2 and add minus 1 extra item to get correct saving
                totalSavings = originalPrice.subtract(itemPrice); // 6.48 - 2.16 = 4.32
                totalSavings = totalSavings.divide(new BigDecimal(count));
            }
            return totalSavings;

        } else {
            BigDecimal totalSavings = null;
            return totalSavings;
        }
    }

    // Determine how an offer should be displayed.
    @Override
    public String toString() {
        return "BogofOffer{" +
                "count=" + count +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
